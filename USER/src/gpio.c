#include "gpio.h"

void gpio_config(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    __RCC_GPIOA_CLK_ENABLE();
    __RCC_GPIOB_CLK_ENABLE();
    __RCC_GPIOF_CLK_ENABLE();

    GPIO_InitStruct.IT = GPIO_IT_NONE;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pins = PW_PIN;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;

    GPIO_Init(PW_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.IT = GPIO_IT_NONE;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pins = LED_RED_PIN | LED_GREEN_PIN | LED_LIGHT_PIN;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;

    GPIO_Init(LED_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.IT = GPIO_IT_NONE;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pins = KEY1_PIN | KEY2_PIN | KEY3_PIN | KEY4_PIN | KEY5_PIN | INOVR_PIN;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;

    GPIO_Init(KEY_PORT, &GPIO_InitStruct);

    //继电器
    GPIO_InitStruct.IT = GPIO_IT_NONE;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pins = ASW4_PIN | CS_CT_PIN | ASW2_PIN | ASW1_PIN | ASW3_PIN;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;

    GPIO_Init(ASW_PORT, &GPIO_InitStruct);

    if (GPIO_ReadPin(KEY_PORT, KEY1_PIN) == GPIO_Pin_RESET)
    {
        GPIO_WritePin(PW_PORT, PW_PIN, GPIO_Pin_SET);         // 开机
        GPIO_WritePin(CS_CT_PORT, CS_CT_PIN, GPIO_Pin_RESET); // cs_ct
        GPIO_WritePin(LED_PORT, LED_RED_PIN, GPIO_Pin_SET);
        GPIO_WritePin(LED_PORT, LED_LIGHT_PIN, GPIO_Pin_RESET); // 关闭照明灯
        ASW1_LOW();
        ASW2_LOW();
        ASW3_LOW();
        ASW4_LOW();
        GPIO_WritePin(LED_PORT, LED_GREEN_PIN, GPIO_Pin_RESET);
    }
    else
    {
        GPIO_WritePin(PW_PORT, PW_PIN, GPIO_Pin_RESET); // 关机
    }
}
void set_led_red(uint8_t sta)
{
    GPIO_WritePin(LED_PORT,LED_RED_PIN,sta);
}
/**
 * @brief Set the led green object
 * 
 * @param sta 0-亮
 */
void set_led_green(uint8_t sta)
{
    GPIO_WritePin(LED_PORT,LED_GREEN_PIN,sta);
}
void set_led_light(uint8_t sta)
{
    GPIO_WritePin(LED_PORT,LED_LIGHT_PIN,sta);
}
void toggle_led_light(void)
{
    GPIO_TogglePin(LED_PORT,LED_LIGHT_PIN);
}
void shutdown(void)
{
    GPIO_WritePin(PW_PORT,PW_PIN,GPIO_Pin_RESET);//开机
}
#define LONG_CNT 20
uint16_t key_cnt[8] = {0};
void keyx_check(GPIO_PinState sta,uint8_t num,uint8_t *flag,uint8_t *result)
{
    if (sta == GPIO_Pin_RESET)
    {
        //按键按下
        *flag |= 1 << num;
        key_cnt[num]++;
    }
    else
    {
        if(*flag & (1 << num))
        {
            *flag &= ~(1 << num);
            if (key_cnt[num] < LONG_CNT)
            {
                *result = num + 1;
            }
            else
            {
                key_cnt[num] = 0;
            }
        }
    }

    if (key_cnt[num] > LONG_CNT)
    {
        // *result = num + 9;
        *result = num + 1;
    }
}
/**
 * @brief 按键扫描 10ms一次
 * 
 * @return uint8_t 
 */
uint8_t key_scan(void)
{
    static uint8_t key_flag;
    uint8_t ret = 0;
    keyx_check(GPIO_ReadPin(KEY_PORT, KEY5_PIN),0,&key_flag,&ret);
    keyx_check(GPIO_ReadPin(KEY_PORT, KEY2_PIN),1,&key_flag,&ret);
    keyx_check(GPIO_ReadPin(KEY_PORT, KEY4_PIN),2,&key_flag,&ret);
    keyx_check(GPIO_ReadPin(KEY_PORT, KEY3_PIN),3,&key_flag,&ret);
    keyx_check(GPIO_ReadPin(KEY_PORT, KEY1_PIN),4,&key_flag,&ret);
    return ret;
}
