#ifndef _PWM_H_
#define _PWM_H_
#include <stdint.h>
#include "main.h"
void PWM_OutputConfig(void);
void set_dc_val(uint16_t val);
uint16_t get_dc_val(void);
uint8_t get_pwm_duty(void);
void set_pwm_duty(uint8_t duty);
uint32_t get_pwm_hz(void);
void set_pwm_hz(uint32_t hz_val);
void set_fm_onoff(uint8_t sta);
#endif
