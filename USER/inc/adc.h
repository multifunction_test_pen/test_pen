#ifndef _ADC_H_
#define _ADC_H_
#include "main.h"
void adc_config(void);
void wait_dma_complete(void);
uint16_t get_bat_val(void);
uint16_t pen_volt(void);
uint16_t get_pen_val(void);
void clear_adc_ref_cnt(void);
uint8_t adc_soft_Calibration(uint8_t multiple);
void clear_pen_cahe(void);
void save_adc_ref_val(void);
void read_adc_ref_val(void);
uint16_t get_vdda_val(void);

extern uint8_t pen_gears;
#endif
