#ifndef _APP_H_
#define _APP_H_
#include "gpio.h"
void set_multiple_xn(uint8_t xn);
void switch_auto_disable(uint8_t sta);
void auto_multiple(void);
void logical_level_mode(void);
void pwm_out_mode(void);
void dc_out_mode(void);
void diode_mode(void);
void scan_shutdown(void);
#endif


